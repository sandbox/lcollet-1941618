<?php

/*
 * Copyright (C) Atos
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Form constructor for the administrationof the module.
 */
function spamblocker_db_admin_form($form, $form_state) {
  // Each element has its own fieldset
  $form['stopforumspam'] = array(
    '#type' => 'fieldset',
    '#title' => 'Stop Forum Spam',
    '#description' => t('Stop Forum Spam is a database which provides lists of spammers that persist in abusing forums and blogs with their scams, ripoffs, exploits and other annoyances*. It provides these lists so that you don\'t have to endure the never ending job of having to moderate, filter and delete their rubbish.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#sollapsed' => FALSE,
  );
  // Activation of this spam check
  $form['stopforumspam']['active'] = array(
    '#title' => t('Active the module'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('spamblocker_db_stopforumspam_active', FALSE),
  );
  // Each element has its own fieldset
  $form['fspamlist'] = array(
    '#type' => 'fieldset',
    '#title' => 'FSpamlist',
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#sollapsed' => FALSE,
  );
  // Activation of this spam check
  $form['fspamlist']['active'] = array(
    '#title' => t('Active the module'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('spamblocker_db_fspamlist_active', FALSE),
  );
  // Give the API KEY
  // Activation of this spam check
  // TODO: add the required element regarding the value of the 'activate' element
  $form['fspamlist']['key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#size' => 35,
    '#maxlength' => 60,
    '#required' => FALSE,
    '#default_value' => variable_get('spamblocker_db_fspamlist_key', 'NULL'),
  );

  // Submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#submit' => array('spamblocker_db_admin_form_submit'),
  );

  return $form;
}

/**
 * Validation of the form
 * @param type $form
 * @param type $form_state 
 */
function spamblocker_db_admin_form_submit($form, &$form_state) {
  unset(
    $form_state['values']['submit'], $form_state['values']['form_id'], $form_state['values']['op'],
    $form_state['values']['form_token'], $form_state['values']['form_build_id']);

  foreach ($form_state['values'] as $module => $values) {
    foreach ($values as $key => $value) {
      variable_set("spamblocker_db_{$module}_{$key}", $value);
    }
  }
  drupal_set_message(t('Configuration Saved'));
}

?>
