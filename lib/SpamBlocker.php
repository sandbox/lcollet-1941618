<?php

/*
 * Copyright (C) Atos
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace SpamBlocker;

require_once 'SpamBlockerProof.php';
require_once 'HttpRequest.php';
require_once 'HttpResponse.php';

/**
 * This abstract method must be extented to implement a spam blocker element 
 */
abstract class SpamBlocker {

  /**
   * This attribute is used to request external services
   */
  protected $http_request;

  public function __construct($proxy_server = NULL, $proxy_port = 8080, $proxy_username = NULL,
    $proxy_password = NULL, $proxy_exceptions = '') {
    $this->http_request = new \SpamBlocker\HttpRequest($proxy_server, $proxy_port, $proxy_username, $proxy_password, $proxy_exceptions);
  }

  /**
   * Method to call the service and to get the result as a SpamBlockerProof result
   * @see \SpamBlocker\SpamBlockerProof
   * @param string $ip
   * @param string $email
   * @param string $username
   * @return \SpamBlocker\SpamBlockerProof
   * @throws \InvalidArgumentException 
   */
  public function isSpam($ip, $email, $username) {
    if (!isset($ip) && !isset($email) && !isset($username))
      throw new \InvalidArgumentException('The email, IP and username cannot be all empty');

    // Check the input parameter
    if (isset($ip) && !filter_var($ip, FILTER_VALIDATE_IP)) {
      throw new \InvalidArgumentException('The IP given is invalid');
    }

    if (isset($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
      throw new \InvalidArgumentException('The email given is invalid');
    }

    if (!isset($username)) {
      throw new \InvalidArgumentException('The username is mandatory');
    }

    // Call the service
    return $this->callService($ip, $email, $username);
  }

  abstract protected function callService($ip, $email, $username);
}

?>
