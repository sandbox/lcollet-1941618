<?php

/*
 * Copyright (C) Atos
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace SpamBlocker;

// Need only to accede SpamBlocker.php which refers all the other ones
require_once 'SpamBlocker.php';

define('STOPFORUMSPAM_API_URL', 'http://www.stopforumspam.com/api?f=json');

/**
 * Implement the SpamBlocker abstract method for the "stop forum spam" service
 */
class StopForumSpamService extends SpamBlocker {

  /**
   * Constructor
   * @param type $proxy_server
   * @param type $proxy_port
   * @param type $proxy_username
   * @param type $proxy_password
   * @param type $proxy_exceptions 
   */
  public function __construct($proxy_server = NULL, $proxy_port = 8080, $proxy_username = NULL,
    $proxy_password = NULL, $proxy_exceptions = '') {
    parent::__construct($proxy_server, $proxy_port, $proxy_username, $proxy_password,
      $proxy_exceptions);
  }

  protected function callService($ip = NULL, $email = NULL, $username = NULL) {
    // Build the request URL
    $requestURL = STOPFORUMSPAM_API_URL;

    if (isset($ip))
      $requestURL .= "&ip={$ip}";
    if (isset($email))
      $requestURL .= "&email={$email}";
    if (isset($username))
      $requestURL .= "&username={$username}";

    $response = $this->http_request->getResponse($requestURL);

    if ($response->code > 0)
      throw new \RuntimeException($response->error);

    // Get the result from JSON
    $json = json_decode($response->data);

    // Map to get the result
    $results = array();
    $results['email'] = $json->email->appears > 0 ? 
      new \SpamBlocker\SpamBlockerProof(TRUE, $json->email->confidence) : 
      new \SpamBlocker\SpamBlockerProof(FALSE);
    $results['ip'] = $json->ip->appears > 0 ? 
      new \SpamBlocker\SpamBlockerProof(TRUE, $json->ip->confidence) : 
      new \SpamBlocker\SpamBlockerProof(FALSE);
    $results['username'] = $json->username->appears > 0 ? 
      new \SpamBlocker\SpamBlockerProof(TRUE, $json->username->confidence) : 
      new \SpamBlocker\SpamBlockerProof(FALSE);

    return \SpamBlocker\SpamBlockerProof::aggregateResults($results);
  }

}

?>
