<?php

/*
 * Copyright (C) Atos
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace SpamBlocker;

define("SB_SMALL_CONFIDENCE_WEIGHT", 1);
define("SB_MIDDLE_CONFIDENCE_WEIGHT", 3);
define("SB_HIGH_CONFIDENCE_WEIGHT", 5);
define("SB_NO_SPAM", 1);
define("SB_NO_SPAM_CONFIDENCE", 0);

/**
 * This class is aimed to manage each proof and give them a weight in oder to be managed 
 */
class SpamBlockerProof {

  /**
   * Indicates if this proof is indicating a spammer
   * @var boolean
   */
  public $lookSpam = FALSE;

  /**
   * The confidence in the proof
   * @var float
   */
  public $confidence = 0;

  /**
   * The weight given to the result
   * Rules
   * - For an item which has no result (weight: 1)
   * - For an item which has a result with low confidence < 30% (weight: 1)
   * - For an item which has a result with a medium confidence 30% < x < 60% (weight: 3)
   * - For an item which has a result with a high confidence > 60% (weight: 5)
   * The weight is used to calculated the global confidence
   * @var integer 
   */
  public $weight = 0;

  /**
   * Constructor
   * @param boolean $lookSpam
   * @param float $confidence 
   */
  public function __construct($lookSpam, $confidence = SB_NO_SPAM_CONFIDENCE) {
    $this->lookSpam = $lookSpam;
    $this->confidence = $confidence;
    $this->calculateWeight();
  }

  /**
   * Getter
   * @param string $variable the name of the variable
   * @return type 
   */
  public function __get($variable) {
    return $this->$variable;
  }

  /**
   * Setter
   * @param string $variable the name of the variable to be set
   * @param mixed $value the value of the variable to be set
   */
//  public function __set($variable, $value) {
//    $this->$variable = $value;
//  }

  /**
   * Method which calculates the weight regarding the different element 
   */
  public function calculateWeight() {
    if (!$this->lookSpam) {
      $this->weight = SB_NO_SPAM;
    } else if ($this->confidence < 30) {
      $this->weight = SB_SMALL_CONFIDENCE_WEIGHT;
    } else if ($this->confidence < 60) {
      $this->weight = SB_MIDDLE_CONFIDENCE_WEIGHT;
    } else {
      $this->weight = SB_HIGH_CONFIDENCE_WEIGHT;
    }
  }

  /**
   * It aggregates the result from different sources in order
   * to have a final result;
   * @param type $results 
   */
  public static function aggregateResults($results) {
    $lookSpam = FALSE;
    $confidence = 0;
    $totalWeight = 0;
    foreach ($results as $name => $value) {
      $lookSpam |= $value->lookSpam;
      $confidence += $value->weight * $value->confidence;
      $totalWeight += $value->weight;
    }
    if ( $totalWeight == 0 ) $totalWeight = 1;
    return new \SpamBlocker\SpamBlockerProof($lookSpam, $confidence/$totalWeight);
  }

}

?>
